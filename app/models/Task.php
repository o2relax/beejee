<?php

namespace app\models;

use app\core\models;

class Task extends models {

    static $table = 'tasks';
    static $perPage = 3;
    static $columns = [
        'username', 'email', 'text', 'status', 'image'
    ];

    public static function getAll($page = 1, $sort = 'id', $order = 'desc') {
        return parent::query('SELECT * FROM ' . self::$table . ' ORDER BY ' . $sort . ' ' . $order . ' LIMIT ' . (($page-1) * self::$perPage) . ', ' . self::$perPage);
    }

    public static function count() {
        return parent::query('SELECT COUNT(*) FROM ' . self::$table)[0]['COUNT(*)'];
    }

}