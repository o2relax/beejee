<?php

namespace app\core;

use app\core\validator\email;
use app\core\validator\image;
use app\core\validator\required;
use app\exceptions\InvalidTokenException;

class validator implements required, email, image
{

    protected $messages = [];
    protected $isValid;
    protected $image = [
        'image/png', 'image/jpg', 'image/gif'
    ];

    public function make(array $data = null, array $rules = [], array $messages = null)
    {

        if (!isset($data['token']) || $data['token'] != app::$token) {
            throw new InvalidTokenException();
        }

        foreach ($rules as $name => $list) {
            foreach (explode('|', $list) as $rule) {
                $this->isValid = true;
                switch ($rule) {
                    case 'required':
                        $this->validRequired($data, $name);
                        break;
                    case 'email':
                        $this->validEmail($name);
                        break;
                    case 'image':
                        $this->validImage($data, $name);
                        break;
                }
                if (!$this->isValid) {
                    $this->messages[$name] = !empty($messages[$name . '.' . $rule]) ? $messages[$name . '.' . $rule] : '';
                }
            }
        }

        return count($this->messages) ? false : true;

    }

    public function messages()
    {

        return $this->messages;

    }

    public function validRequired($data, $name)
    {
        return !empty($data[$name]) ?: $this->isValid = false;
    }

    public function validEmail($name)
    {
        return filter_input(INPUT_POST, $name, FILTER_VALIDATE_EMAIL) ?: $this->isValid = false;
    }

    function validImage($data, $name) {
        return in_array(mime_content_type($data[$name]['tmp_name']), $this->image) ?: $this->isValid = false;
    }

}