<?php

namespace app\core;

class requests {

    protected $request = [];

    public function post($key = null, $default = null) {

        if($key) {
            return empty($_POST[$key]) ? $default ? $default : null : $this->escape($_POST[$key]);
        } else {
            $array = [];
            foreach($_POST as $k => $v) {
                $array[$k] = $this->escape($v);
            }
            return $array;
        }

    }

    public function get($key = null, $default = null) {

        if($key) {
            return empty($_GET[$key]) ? $default ? $default : null : $this->escape($_GET[$key]);
        } else {
            $array = [];
            foreach($_GET as $k => $v) {
                $array[$k] = $this->escape($v);
            }
            return $array;
        }

    }

    public function file($key = null) {

        if($key) {
            return !empty($_FILES[$key]) ? $_FILES[$key] : null;
        }

    }

    public function filter($key) {

        if($this->get('sort') == $key) {
            return $this->get('order') == 'asc' ? 'desc' : 'asc';
        } else {
            return 'desc';
        }

    }

    public function server($key) {

        return empty($_SERVER[$key]) ? null : $_SERVER[$key];

    }

    public function escape($value) {

        return htmlspecialchars(stripslashes(trim($value)));

    }

}