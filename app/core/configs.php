<?php

namespace app\core;

class configs {

    protected $config = [];

    public function __construct() {

        $this->config = include CONFIG_PATH . DIRECTORY_SEPARATOR . 'common.php';

    }

    public function all() {

        return $this->config;

    }

    public function get($code) {

        return empty($this->config[$code]) ? null : $this->config[$code];

    }

}