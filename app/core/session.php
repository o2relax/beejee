<?php

namespace app\core;

class session
{

    protected $session = [];

    public function get($key = null, $default = null)
    {

        if ($key) {
            return empty($_SESSION[$key]) ? $default ? $default : null : $this->escape($_SESSION[$key]);
        } else {
            $array = [];
            foreach ($_SESSION as $k => $v) {
                $array[$k] = $this->escape($v);
            }
            return $array;
        }

    }

    public function put($key = null, $value = null)
    {
        return $_SESSION[$key] = $this->escape($value);
    }

    public function delete($key)
    {
        unset($_SESSION[$key]);
    }

    public function auth()
    {
        return empty($_SESSION['auth']) ? false : true;
    }

    public function user()
    {
        return empty($_SESSION['user']) ? false : $_SESSION['user'];
    }

    public function isAdmin($id)
    {
        return !$this->auth() || empty($_SESSION['user']['id']) ? false : $_SESSION['user']['id'] == $id ? true : false;
    }

    public function logout()
    {
        session_destroy();
    }

    public function escape($value)
    {

        if (is_array($value)) {
            $array = [];
            foreach ($value as $k => $v) {
                $array[$k] = htmlspecialchars(stripslashes(trim($v)));
            }

            return $array;
        } else {
            return htmlspecialchars(stripslashes(trim($value)));
        }


    }

}