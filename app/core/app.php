<?php

namespace app\core;

use Exception;

class app
{

    public static $router;

    public static $db;

    public static $kernel;

    public static $token;

    public static function init()
    {
        spl_autoload_register(['static', 'loadClass']);
        static::bootstrap();
        set_exception_handler('handleException');
    }

    public static function bootstrap()
    {
        session_start();
        static::$router = new router();
        static::$kernel = new kernel();
        static::$db = new db();

    }

    public static function loadClass($className)
    {
        $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        require_once ROOT_PATH . DIRECTORY_SEPARATOR . $className . '.php';
    }

}