<?php

return [
    'app' => 'BeeJee',
    'templates' => 'templates',
    'templates_cache' => 'cache',
    'admin_id' => 1,
    'secret' => 'blablabla',
    'image_size' => [100, 100]
];