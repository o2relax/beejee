<?php

return [
    'type' => 'mysql',
    'host' => 'localhost',
    'name' => 'beejee',
    'charset' => 'utf8',
    'user' => 'homestead',
    'password' => 'secret'
];