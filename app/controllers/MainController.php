<?php

namespace app\controllers;

use app\core\Controller;
use app\core\pagination;
use app\core\validator;
use app\models\Task;
use app\models\User;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;

class MainController extends Controller
{

    protected $isAdmin = false;

    public function __construct()
    {
        parent::__construct();
        $this->addVar('title', $this->config->get('app'));
        $this->addVar('auth', $this->session->auth());
        $this->addVar('user', $this->session->user());
        $this->isAdmin = $this->session->isAdmin($this->config->get('admin_id'));
        $this->addVar('isAdmin', $this->isAdmin);
    }

    public function index()
    {
        $filter = [
            'username' => url('/', ['sort' => 'username', 'order' => $this->request->filter('username')]),
            'email' => url('/', ['sort' => 'email', 'order' => $this->request->filter('username')]),
            'status' => url('/', ['sort' => 'status', 'order' => $this->request->filter('username')]),
        ];

        $pagination = new pagination();
        $total = Task::count();
        $items = Task::getAll($this->request->get('page', 1), $this->request->get('sort', 'id'), $this->request->get('order', 'desc'));

        return $this->view('page.list', [
            'tasks' => $items,
            'sort' => $this->request->get('sort'),
            'order' => $this->request->get('order'),
            'filter' => $filter,
            'pagination' => $pagination->render(
                $total,
                Task::$perPage,
                $this->request->get('page', 1),
                $this->request->get()
            ),
            'total' => $total
        ]);
    }

    public function login()
    {

        if ($this->session->auth()) {
            return redirect('/');
        }

        $errors = $this->session->get('errors');
        $old = $this->session->get('old');
        $this->session->delete('errors');
        $this->session->delete('old');

        return $this->view('page.login', [
            'old' => $old,
            'errors' => $errors,
        ]);

    }

    public function postLogin()
    {

        $validator = new validator();

        $rules = [
            'login' => 'required',
            'password' => 'required',
        ];

        $messages = [
            'login.required' => 'Login is required',
            'password.required' => 'Password is required'
        ];

        $this->session->put('old', $this->request->post());

        if ($validator->make($this->request->post(), $rules, $messages)) {

            $user = User::makeAuth($this->request->post('login'), md5($this->request->post('password')));

            if (!$user) {
                $this->session->put('errors', [
                    'login' => 'Wrong login or password',
                    'password' => 'Wrong login or password'
                ]);
                return redirect('/main/login');
            }

            $token = md5(rand());

            $this->session->put('auth', true);
            $this->session->put('token', $token);
            $this->session->put('user', [
                'login' => $user['login'],
                'id' => $user['id']
            ]);

            User::updateToken($user['id'], $token);

            $this->session->delete('old');

            return redirect('/');

        } else {
            $this->session->put('errors', $validator->messages());
            return redirect('/main/login');
        }

    }

    public function logout()
    {

        $this->session->logout();

        return redirect('/');

    }

    public function edit($id)
    {

        $task = Task::getById($id);

        if (!$task || !$this->isAdmin) {
            return redirect('/');
        }

        $errors = $this->session->get('errors');
        $old = $this->session->get('old');
        $this->session->delete('errors');
        $this->session->delete('old');

        return $this->view('task.edit', [
            'task' => $task,
            'old' => $old,
            'errors' => $errors
        ]);

    }

    public function postEdit($id)
    {

        if (!$this->isAdmin) {
            return redirect('/');
        }

        $validator = new validator();

        $rules = [
            'text' => 'required',
        ];

        $messages = [
            'text.required' => 'text is required',
        ];

        $this->session->put('old', $this->request->post());

        if ($validator->make($this->request->post(), $rules, $messages)) {

            Task::update($id, array_only($this->request->post(), Task::$columns));

            $this->session->delete('old');

            return redirect('/main/edit/' . $id);
        } else {
            $this->session->put('errors', $validator->messages());
            return redirect('/main/edit/' . $id);
        }

    }

    public function create()
    {

        $errors = $this->session->get('errors');
        $old = $this->session->get('old');
        $this->session->delete('errors');
        $this->session->delete('old');

        return $this->view('task.create', [
            'old' => $old,
            'errors' => $errors
        ]);

    }

    public function postCreate()
    {

        $validator = new validator();

        $rules = [
            'username' => 'required',
            'email' => 'required|email',
            'text' => 'required',
            'image' => 'image'
        ];

        $messages = [
            'username.required' => 'text is required',
            'email.required' => 'email is required',
            'email.email' => 'email must be email',
            'text.required' => 'text is required',
            'image.image' => 'only png,jpg,gif',
        ];

        $this->session->put('old', $this->request->post());

        $image = $this->request->file('image');

        $input = array_merge($this->request->post(), [
            'image' => $image
        ]);

        if ($validator->make($input, $rules, $messages)) {

            $imagine = new Imagine();
            $size    = new Box(...$this->config->get('image_size'));
            $mode    = ImageInterface::THUMBNAIL_INSET;

            $image_name = md5(uniqid() . time()) . '.' . pathinfo($image['name'])['extension'];
            $path = WWW_PATH . '/assets/img/' . $image_name;

            $imagine->open($image['tmp_name'])
                ->thumbnail($size, $mode)
                ->save($path);

            $data = array_merge($this->request->post(), [
                'image' => $image_name,
            ]);

            Task::create(array_only($data, Task::$columns));

            $this->session->delete('old');

            return redirect('/');
        } else {
            $this->session->put('errors', $validator->messages());
            return redirect('/main/create/');
        }

    }

}